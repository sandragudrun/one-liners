
var jokes = [
    {
        "question": "<h2>What's the best thing about Switzerland?</h2>",
        "answer": "<h3>- I don't know, but the flag is a big plus.</h3>"
    },
    {
        "question": "<h2>Helvetica and Times New Roman walk into a bar.</h2>",
        "answer": `<h3>- "Get out of here!" shouts the bartender. "We don't serve your type!"</h3>`
    },
    {
        "question": "<h2>What’s the difference between ignorance and apathy?</h2>",
        "answer": "<h3>- I don't know and I don't care </h3>"
    },
    {
        "question": "<h2>My mom loves gardening. She was so excited that spring finally arrived!</h2>",
        "answer": `<h3>- "She wet her plants!"</h3>`
    },
    {
        "question": "<h2>Why did the crab never share?</h2>",
        "answer": `<h3>- Because he’s shellfish</h3>`
    },

];

const createJoke = function () {
    for (var i = 0; i < jokes.length; i++) {
        var joke = `<div class="joke-holder">` + jokes[i].question + jokes[i].answer + `</div>`;
        document.querySelector("#jokes").innerHTML += joke;
    }
}
createJoke();


this === window ? null : module.exports = {
    jokes,
};
